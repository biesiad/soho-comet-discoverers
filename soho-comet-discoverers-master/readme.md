# SOHO Comet Discoverers

Extension of the project [nasa-comets](https://github.com/mbiesiad/nasa-comets). ⭐

## Content
* [Intro](#intro)
* [Summary for SOHO](#summary-for-soho)
* [Summary for STEREO](#summary-for-stereo)
* [Disclaimer](#disclaimer)
* [Contributing](#contributing)
* [Code of Conduct](#code-of-conduct)
* [License](#license)

## Intro

Data from [nasa-comets](https://github.com/mbiesiad/nasa-comets), also from the official [Sungrazer site](https://sungrazer.nrl.navy.mil/).

Please treat it more as a `preview`. If you see something incorrect, feel free to contact me or create an issue/PR.
`data-set` files here are mostly partial. The full data-sets can be found in `summary`.

Listed discoverers & their SOHO discoveries. Credits: ESA/NASA SOHO/LASCO, Sungrazer Project

## Summary for SOHO

* Until SOHO-4000 - **sungrazers** (Kreutz + NonKreutz: Meyer, Kracht, Kracht II, Marsden, below):

o [TABLE](https://github.com/mbiesiad/soho-comet-discoverers/blob/master/summary/sungrazers-1.csv)

o [SORTED TABLE](https://github.com/mbiesiad/soho-comet-discoverers/blob/master/summary/sungrazers-1-sorted.csv)

* Until SOHO-4000 - for **Kreutz** group

The summary until **SOHO-4000** is available [HERE](https://github.com/mbiesiad/soho-comet-discoverers/blob/master/summary/until-SOHO-4000.csv).

Sorted table, [here](https://github.com/mbiesiad/soho-comet-discoverers/blob/master/summary/until-SOHO-4000-sorted.csv).

Included data from 1996 to 2020 (June).

*Note: Not necessary Σ to 4k etc (please remember about co-discoveries).*

* Until SOHO-4000 - for **NonKreutz** group, [here](https://github.com/mbiesiad/soho-comet-discoverers/tree/master/NonKreutz)

- [Meyer](https://github.com/mbiesiad/soho-comet-discoverers/tree/master/NonKreutz/Meyer)

o [table](https://github.com/mbiesiad/soho-comet-discoverers/blob/master/NonKreutz/Meyer/summary/summary-1.csv)
- [Marsden](https://github.com/mbiesiad/soho-comet-discoverers/tree/master/NonKreutz/Marsden)

o [table](https://github.com/mbiesiad/soho-comet-discoverers/blob/master/NonKreutz/Marsden/summary/summary-1.csv)

o [sorted table](https://github.com/mbiesiad/soho-comet-discoverers/blob/master/NonKreutz/Marsden/summary/summary-1-sorted.csv)
- [Kracht](https://github.com/mbiesiad/soho-comet-discoverers/tree/master/NonKreutz/Kracht)

o [table](https://github.com/mbiesiad/soho-comet-discoverers/blob/master/NonKreutz/Kracht/summary/summary-1.csv)
- [Kracht-II](https://github.com/mbiesiad/soho-comet-discoverers/tree/master/NonKreutz/Kracht-II)

o [table](https://github.com/mbiesiad/soho-comet-discoverers/blob/master/NonKreutz/Kracht-II/summary/summary-1.csv)

Please remember - there is no included 'NonGroup' comets for instance and the data depends of data from the site (sometimes are not added yet), so the numbers may be sometimes little different. Published on date: 2020-7-9

---

Congratulations to you all! ⭐

## Summary for STEREO

See [here](https://github.com/mbiesiad/soho-comet-discoverers/tree/master/STEREO).

The summary is [here](https://github.com/mbiesiad/soho-comet-discoverers/blob/master/STEREO/summary/based-on-data-1.csv).

Sorted table, [here](https://github.com/mbiesiad/soho-comet-discoverers/blob/master/STEREO/summary/based-on-data-1-sorted.csv).

## Disclaimer
I can't guarantee a 100% completeness and correctness of the data. I expressly reject any claims against me due to incorrect data (see sources).

*If you want delete your name - please create an issue or contact with me directly.*

PS. It's not an official Project repo.

## Contributing

Warmly welcome! Kindly go through [Contribution Guidelines](CONTRIBUTING.md) first.

## Code of Conduct

Examples of behavior that contributes to creating a positive environment include:

    Using welcoming and inclusive language
    Being respectful of differing viewpoints and experiences
    Gracefully accepting constructive criticism
    Focusing on what is best for the community
    Showing empathy towards other community members

## License
Free [MIT](LICENSE) license.

__________________________________________________

Reports by @[Karl Battams](https://twitter.com/SungrazerComets) (Dr Sungrazer)

Created by @[mbiesiad](https://github.com/mbiesiad) & supported by awesome [contributors](https://github.com/mbiesiad/soho-comet-discoverers/graphs/contributors)! 🚀

See also another project about [nasa-comets](https://github.com/mbiesiad/nasa-comets) or [my discoveries](https://github.com/mbiesiad/discoveries-biesiada).

Last updated: 8/7/2020
